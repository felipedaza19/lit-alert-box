import { css, } from 'lit-element';

export default css`:host {
  display: block;
  width: 100vw;
  font-family: var(--lit-fontDefault, sans-serif);
  font-size: 12px; }

#header {
  flex: 1;
  margin: 0;
  color: var(--lit-color-header, #ffffff);
  background-color: var(--lit-background-color-header, #06457F); }

#body {
  text-align: center;
  min-height: 28.125rem;
  max-height: 4.375rem; }

#footer {
  min-height: 7rem;
  max-height: 4.375rem; }

cells-icon-message .icon-message-first {
  background-color: var(--bbva-white, #ffffff); }

#title {
  overflow: hidden;
  text-align: center;
  max-width: 75%;
  margin: auto;
  line-height: 1;
  padding: 1rem 0 1rem 1rem;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 18px; }

cells-icon {
  position: absolute;
  top: 15;
  bottom: 0;
  margin: 0;
  left: 90%; }

.subtitle {
  font-weight: bold;
  margin-top: 3rem;
  margin-bottom: 0.5rem;
  font-size: var(--lit-text-size, 1rem);
  color: var(--lit-color-subtitle, #2C3853); }

.icon-container {
  margin: 2.25rem 0 0.375rem; }

img {
  margin-top: 1rem;
  width: 15rem;
  height: auto; }

.content-container {
  position: relative;
  flex: 1;
  font-size: var(--lit-text-size-14, 0.875rem); }

.buttons-container {
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%; }

bbva-button-default {
  display: block;
  width: 2rem;
  margin-bottom: 1rem; }

span {
  display: flex;
  text-align: center;
  justify-content: center;
  font-size: 0.85em;
  font-weight: lighter;
  line-height: 1.2rem; }

.secondary {
  background-color: #237ABA;
  color: #ffffff; }

.link {
  background-color: inherit;
  color: #518CBF; }

.disabled {
  pointer-events: none;
  background-color: #E9E9E9;
  border-style: none;
  color: #ffffff; }
`;