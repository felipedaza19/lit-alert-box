import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import '@cells-components/cells-icon-message';
import '@bbva-web-components/bbva-button-default';
import '@cells-components/cells-icon';
import styles from './lit-alert-box-styles.js';

/**
This component ...

Example:

```html
<lit-alert-box></lit-alert-box>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class LitAlertBox extends LitElement {
  static get is() {
    return 'lit-alert-box';
  }

  // Declare properties
  static get properties() {
    return {
      fullHeight: { type: Boolean, },
      iconOnlyFullHeight: { type: Boolean, },
      opened: { 
        type: Boolean,
        notify: true,
        reflecToAttribute: true,
        attribute: '_checkedOpened'
      },
      title: { type: String, },
      imageAsTitle: { type: String, },
      imageAsTitleAlt: { type: String, },
      imageAsTitleWidth: { type: Number },
      subtitle: { type: String },
      message: { type: String },
      heroImage: { type: String },
      acceptButton: { type: String },
      acceptButtonInfo: { type: String },
      cancelButton: { type: String },
      cancelButtonInfo: { type: String },
      iconPositionConfig: { type: Object },
      closeIconId: { type: String },
      closeIconSize: { type: Number },
      disablePrimaryButton: { type: String },
      disableSecondaryButton: { type: String },
      primaryButtonClass: { type: String },
      closeOnPrimaryButton: { type: Boolean},
      closeOnSecondaryButton: { type: Boolean },
      hideCloseIcon: { type: Boolean }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.fullHeight = false;
    this.iconOnlyFullHeight = false;
    this.opened = false;
    this.imageAsTitleAlt = '';
    this.imageAsTitleWidth = 50;
    this.closeIconId = 'coronita:close';
    this.closeIconSize = 18;

    this.disablePrimaryButton = '';
    this.disableSecondaryButton = '';

    this.primaryButtonClass = 'secondary';
    this.closeOnPrimaryButton = false;
    this.closeOnSecondaryButton = false;
    this.hideCloseIcon = false;

    this.title = '';
    this.imageAsTitle = '';
    this.subtitle = '';
    this.heroImage = './images/general_error.svg';
    this.iconPositionConfig = {type:'', icon:'', message:'La operación ha fallado. Por favor, inténtalo de nuevo.' };
    this.message = '';
    this.acceptButton = '';
    this.acceptButtonInfo = '';
    this.cancelButton = '';
    this.cancelButtonInfo = '';
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('lit-alert-box-shared-styles').cssText}
    `;
  }


  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>

      <section id="header">
        ${this.title?
          html `<header id="title" heading="" slot="heading">${this.title}</header>`:''
        }
        
        <cells-icon icon="${this.closeIconId}" @click="${this._onClose}" disabled></cells-icon>
      </section>
      
      <section id="body">

        ${this.heroImage?
          html `<div class="icon-container">
                  <img src="${this.heroImage}" alt="">
                </div>`:''
        }

        ${this._hasImageAsTitle(this.title, this.imageAsTitle)?
          html`<img 
                  id="imageAsTitle" 
                  src="{this.imageAsTitle}" 
                  alt="{this.imageAsTitleAlt}" 
                  width="${this.imageAsTitleWidth}" 
                  slot="heading"></img>` : ''
        }

        ${this.subtitle? 
          html `<h2 class="subtitle">${this.subtitle}</h2>`:''
        }
        
        <div class="content-container">
          ${this.message?
            html `<div inner-h-t-m-l="${this.message}"></div>`:''
          }
        </div>
        <slot></slot>

        ${this.iconPositionConfig?
          html `<cells-icon-message
                  type="${this.iconPositionConfig.type}"
                  icon="${this.iconPositionConfig.icon}"
                  message="${this.iconPositionConfig.message}"
                  class="icon-message-first"></cells-icon-message>`:''
        }
      </section>
      
      <section id="footer">
        <section class="buttons-container" slot="buttons">
          ${this.acceptButton?
            html `<bbva-button-default class="btn ${this.primaryButtonClass} ${this.disablePrimaryButton}" id="acceptButton" @click="${this._onAcceptClick}">
                      ${this.acceptButton}
                      ${this.acceptButtonInfo?
                        html `<span>${this.acceptButtonInfo}</span>`:''}
                    
                  </bbva-button-default>`:''}

          ${this.cancelButton?
            html `<bbva-button-default class="link ${this.disableSecondaryButton}" id="cancelButton" @click="${this._onCancelClick}">
                      ${this.cancelButton}
                        ${this.cancelButtonInfo?
                          html `<span>${this.cancelButtonInfo}</span>`:''}
                    
                  </bbva-button-default>`:''}
        </section>
      </section>
    `;
  }

  open() {
    this.opened = true;
  }

  close() {
    this.opened = false;
  }

  _onClose(e) {
    this.opened = false;
    if (e.detail.canceled) {
      this._fireCancel(true);
    }
    console.log('Press X -> _onClose');
    console.log(this.disablePrimaryButton);
    console.log(this.disableSecondaryButton);
    
    
  }

  _fireCancel(canceled) {
    this.dispatchEvent(new CustomEvent('cancel', {
      bubbles: true,
      composed: true,
      detail: { cancelFromHeader: canceled }
    }));
  }

  _onCancelClick() {
    this._fireCancel(false);
    if (this.closeOnSecondaryButton) {
      this.opened = false;
    }
    console.log('Press Cancel');
  }

  _onAcceptClick() {
    this.dispatchEvent(new CustomEvent('accept', {
      bubbles: true,
      composed: true
    }));
    if (this.closeOnPrimaryButton) {
      this.opened = false;
    }
    console.log('press accept');
  }

  _checkedOpened(opened) {
    this.shadowRoot.getElementById('modal')[opened ? 'open' : 'close']();
    //this.$.modal[opened ? 'open' : 'close']();
  }

  _hasImageAsTitle(title, imageAstitle) {
    return title && imageAstitle;
  }
}

// Register the element with the browser
customElements.define(LitAlertBox.is, LitAlertBox);
