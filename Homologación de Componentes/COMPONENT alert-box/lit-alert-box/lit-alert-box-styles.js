import { css, } from 'lit-element';

export default css`:host {
  visibility: hidden;
  box-sizing: border-box;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  text-align: center;
  z-index: var(--bbva-z-index-notifications, 1);
  background: var(--bbva-panel-button-fixed-background, linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, white 5%, white 100%));
  will-change: visibility, transform;
  transform-origin: 0 0;
  transform: translateY(100vh);
  transition: visibility 0s linear 300ms, transform ease-in-out var(--bbva-panel-button-fixed-transition-time, 0.2s);
  @apply --bbva-panel-button-fixed; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

:host([show]) {
  visibility: visible;
  transition-delay: 0s;
  outline: 0;
  transform: translateY(0); }

:host(.no-gradient) {
  padding: 0;
  --bbva-panel-button-fixed-background: var(--bbva-white, #ffffff);
  @apply --bbva-panel-button-fixed-no-gradient; }

:host {
  display: block;
  width: 100vw;
  height: 100vh;
  font-family: var(--lit-fontDefault, sans-serif);
  font-size: var(--lit-fontsizeDefault, 12px);
  @apply --lit-alert-box; }

#header {
  flex: 1;
  margin: 0;
  margin-bottom: 5%;
  color: var(--lit-color-header, #ffffff);
  background-color: var(--lit-background-color-header, #06457F);
  @apply --lit-alert-box-header; }

#body {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: var(--lit-height-body, 70%);
  margin-bottom: var(--lit-margin-bottom-body, 5%);
  @apply --lit-alert-box-body; }

#footer {
  height: var(--lit-height-footer, 16%);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  @apply --lit-alert-box-footer; }

.icon-message-first {
  padding-bottom: var(--lit-pb-icon-message-first, 60%);
  background-color: var(--lit-bg-icon-message-first, #ffffff);
  @apply --lit-alert-box-icon-message-first; }

#title {
  overflow: var(--lit-overflow-title, hidden);
  text-align: var(--lit-text-align-title, center);
  max-width: var(--lit-max-width-title, 75%);
  margin: var(--lit-margin-title, auto);
  line-height: var(--lit-line-height-title, 1);
  padding: var(--lit-padding-title, 1rem 0.5rem 1rem 1rem);
  white-space: var(--lit-white-space-title, nowrap);
  text-overflow: var(--lit-text-overflow-title, ellipsis);
  font-size: var(--lit-font-size-title, 18px);
  @apply --lit-alert-box-title; }

cells-icon {
  position: var(--lit-position-icon-icon, absolute);
  top: var(--lit-top-icon-icon, 15);
  bottom: var(--lit-bottom-icon-icon, 0);
  margin: var(--lit-margin-icon-icon, 0);
  left: var(--lit-left-icon-icon, 90%);
  @apply --lit-alert-box-cells-icon; }

.subtitle {
  font-weight: var(--lit-font-weigth-subtitle, bold);
  margin-bottom: var(--lit-margin-bottom-subtitle, 0.5rem);
  font-size: var(--lit-text-size-subtitle, 1rem);
  color: var(--lit-color-subtitle, #2C3853);
  @apply --lit-alert-box-subtitle; }

.icon-container {
  margin: var(--lit-margin-icon-container, 2.25rem 0 0.375rem);
  @apply --lit-alert-box-icon-container; }

img {
  margin-top: var(--lit-margin-top-img, 10%);
  width: var(--lit-width-img, 15rem);
  height: var(--lit-height-img, auto);
  @apply --lit-alert-box-img; }

.content-container {
  position: var(--lit-position-content-container, relative);
  flex: var(--lit-flex-content-container, 1);
  font-size: var(--lit-font-size-content-container, 0.875rem);
  @apply --lit-alert-box-content-container; }

.buttons-container {
  display: var(--lit-display-buttons-container, flex);
  flex-direction: var(--lit-flex-direction-buttons-container, column);
  margin: 0;
  width: var(--lit-width-buttons-container, 100%);
  @apply --lit-alert-box-buttons-container; }
  .buttons-container bbva-button-default {
    font-size: var(--lit-font-size-bbva-button-default, 0.9375rem);
    box-sizing: content-box;
    font-weight: 500;
    display: block;
    min-width: var(--lit-alert-box-min-width-bbva-button-default, 4rem);
    margin: 0 auto var(--lit-alert-box-margin-bbva-button-default, 0.5rem);
    padding-left: var(--lit-alert-box-padding-left-bbva-button-default, 2rem);
    padding-right: var(--lit-alert-box-padding-right-bbva-button-default, 2rem);
    text-transform: none;
    @apply --lit-alert-box-bbva-button-default; }

span {
  display: var(--lit-display-span, flex);
  text-align: var(--lit-text-align-span, center);
  justify-content: var(--lit-justify-content-span, center);
  font-size: var(--lit-font-size-span, 0.85em);
  font-weight: var(--lit-font-weight-span, lighter);
  line-height: var(--lit-line-height-span, 1.2rem);
  @apply --lit-alert-box-span; }

.secondary {
  background-color: var(--lit-background-color-secondary, #237ABA);
  color: var(--lit-color-secondary, #ffffff);
  @apply --lit-alert-box-secondary; }

.link {
  background-color: var(--lit-background-color-link, inherit);
  color: var(--lit-color-link, #518CBF);
  @apply --lit-alert-box-link; }

.disabled {
  pointer-events: var(--lit-pointer-events-disabled, none);
  background-color: var(--lit-background-color-disabled, #E9E9E9);
  border-style: var(--lit-background-style-disabled, none);
  color: var(--lit-color-disabled, #ffffff);
  @apply --lit-alert-box-disabled; }
`;