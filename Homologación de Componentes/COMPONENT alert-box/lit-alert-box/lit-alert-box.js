import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import '@cells-components/cells-icon-message';
import '@bbva-web-components/bbva-button-default';
import '@cells-components/cells-icon';
import '@bbva-web-components/bbva-header-main';
import styles from './lit-alert-box-styles.js';

/**
This component ...

Example:

```html
  <lit-alert-box 
    title="Error" 
    subtitle="Operation not performed"
    acceptButton="Retry"
    acceptButtonInfo="Try again" 
    cancelButton="Go Out"
    cancelButtonInfo="Exit the process">
  </lit-alert-box>
```

## Styling
The following custom properties and mixins are available for styling:

### Custom Properties
----------------------------------------------------------------------------------------------------------------
| Selector                         | CSS Property     | CSS Variable                                           |   
|:---------------------------------|:-----------------|:-------------------------------------------------------|
| :host                            | font-family      | --lit-fontDefaul                                       |
| :host                            | background-color | --lit-fontsizeDefault                                  |
| #header                          | color            | --lit-color-header                                     |
| #header                          | background-color | --lit-background-color-header                          |
| #body                            | height           | --lit-height-body                                      | 
| #body                            | margin-bottom    | --lit-margin-bottom-body                               | 
| #footer                          | height           | --lit-height-footer                                    | 
| icon-message-first               | background-color | --lit-bg-icon-message-first                            | 
| icon-message-first               | padding-bottom   | --lit-pb-icon-message-first                            | 
| #title                           | overflow         | --lit-overflow-title                                   | 
| #title                           | text-align       | --lit-text-align-title                                 | 
| #title                           | max-width        | --lit-max-width-title                                  | 
| #title                           | margin           | --lit-margin-title                                     | 
| #title                           | line-height      | --lit-line-height-title                                | 
| #title                           | padding          | --lit-padding-title                                    | 
| #title                           | white-space      | --lit-white-space-title                                | 
| #title                           | text-overflow    | --lit-text-overflow-title                              | 
| #title                           | font-size        | --lit-font-size-title                                  | 
| cells-icon                       | position         | --lit-position-icon-icon                               | 
| cells-icon                       | top              | --lit-top-icon-icon                                    | 
| cells-icon                       | bottom           | --lit-bottom-icon-icon                                 | 
| cells-icon                       | margin           | --lit-margin-icon-icon                                 | 
| cells-icon                       | left             | --lit-left-icon-icon                                   | 
| .subtitle                        | font-weight      | --lit-font-weigth-subtitle                             | 
| .subtitle                        | margin-bottom    | --lit-margin-bottom-subtitle                           | 
| .subtitle                        | font-size        | --lit-text-size-subtitle                               | 
| .subtitle                        | color            | --lit-color-subtitle                                   | 
| .icon-container                  | margin           | --lit-margin-icon-container                            | 
| img                              | margin-top       | --lit-margin-top-img                                   | 
| img                              | width            | --lit-width-img                                        | 
| img                              | height           | --lit-height-img                                       | 
| .content-container               | height           | --lit-position-content-container                       | 
| .content-container               | flex             | --lit-flex-content-container                           |
| .content-container               | display          | --lit-display-buttons-container                        | 
| .buttons-container               | flex-direction   | --lit-flex-direction-buttons-container                 | 
| .buttons-container               | display          | --lit-display-buttons-container                        | 
| .buttons-container               | width            | --lit-width-buttons-containe                           | 
| bbva-button-default              | font-size        | --lit-font-size-bbva-button-default                    | 
| bbva-button-default              | min-width        | --lit-alert-box-min-width-bbva-button-default          | 
| bbva-button-default              | margin           | --lit-alert-box-margin-bbva-button-default             | 
| bbva-button-default              | padding-left     | --lit-alert-box-padding-left-bbva-button-default       | 
| bbva-button-default              | padding-right    | --lit-alert-box-padding-right-bbva-button-default      | 
| span                             | display          | --lit-display-span                                     | 
| span                             | text-align       | --lit-text-align-span                                  | 
| span                             | justify-content  | --lit-justify-content-span                             | 
| span                             | font-size        | --lit-font-size-span                                   | 
| span                             | font-weight      | --lit-font-weight-span                                 | 
| span                             | line-height      | --lit-line-height-spa                                  | 
| .secondary                       | background-color | --lit-background-color-secondaryr                      | 
| .secondary                       | color            | --lit-color-secondary                                  | 
| .link                            | background-color | --lit-background-color-link                            | 
| .link                            | color            | --lit-color-link                                       | 
| .disabled                        | pointer-events   | --lit-pointer-events-disabled                          | 
| .disabled                        | background-color | --lit-background-color-disabled                        | 
| .disabled                        | border-style     | --lit-background-style-disabledr                       | 
| .disabled                        | color            | --lit-color-disabled                                   | 
----------------------------------------------------------------------------------------------------------------


### @apply
------------------------------------------------------------------------------------
| Mixins                                        | Selector                         |
| --------------------------------------------- | -------------------------------- |
| --lit-alert-box                               | :host                            |
| --lit-alert-box-header                        | #header                          |
| --lit-alert-box-body                          | #body                            |
| --lit-alert-box-footer                        | #footer                          |
| --lit-alert-box-icon-message-first            | .icon-message-first              |
| --lit-alert-box-title                         | #title                           |
| --lit-alert-box-cells-icon                    | cells-icon                       |
| --lit-alert-box-subtitle                      | .subtitle                        |
| --lit-alert-box-icon-container                | .icon-container                  |
| --lit-alert-box-img                           | img                              |
| --lit-alert-box-content-container             | .content-container               |
| --lit-alert-box-buttons-container             | .buttons-container               |
| --lit-alert-box-bbva-button-default           | bbva-button-default              |
| --lit-alert-box-span                          | span                             |
| --lit-alert-box-secondary                     | .secondary                       |
| --lit-alert-box-link                          | .link                            |
| --lit-alert-box-disabled                      | .disabled                        |
------------------------------------------------------------------------------------



* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class LitAlertBox extends LitElement {
  static get is() {
    return 'lit-alert-box';
  }

  // Declare properties
  static get properties() {
    return {
      title: { type: String, },
      subtitle: { type: String },
      message: { type: String },
      heroImage: { type: String },
      acceptButton: { type: String },
      acceptButtonInfo: { type: String },
      cancelButton: { type: String },
      cancelButtonInfo: { type: String },
      closeIconId: { type: String },
      disablePrimaryButton: { type: String },
      disableSecondaryButton: { type: String },
      primaryButtonClass: { type: String },
      show: {
        type: Boolean,
        reflect: true
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.closeIconId = 'coronita:close';
    this.disablePrimaryButton = '';
    this.disableSecondaryButton = '';
    this.primaryButtonClass = 'secondary';
    this.title = '';
    this.subtitle = '';
    this.heroImage = './images/general_error.svg';
    this.message = 'The operation has failed. please try again.';
    this.acceptButton = '';
    this.acceptButtonInfo = '';
    this.cancelButton = '';
    this.cancelButtonInfo = '';
    this.show = false;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('lit-alert-box-shared-styles').cssText}
    `;
  }


  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>

      <section aria-hidden="${String(!this.show)}">
        <section id="header">
          
          ${this.title?
            html `<bbva-header-main text="${this.title}"></bbva-header-main>`:''
          }

          <cells-icon icon="${this.closeIconId}" @click="${this._onCancelClick}"></cells-icon>
        </section>
        
        <section id="body">
          ${this.heroImage?
            html `<div class="icon-container">
                    <img src="${this.heroImage}" alt="">
                  </div>`:''
          }

          ${this.subtitle? 
            html `<h2 class="subtitle">${this.subtitle}</h2>`:''
          }
          
          <div class="content-container">
            ${this.message?
              html `<div inner-h-t-m-l="${this.message}"></div>`:''
            }
          </div>

          ${this.message?
            html `<cells-icon-message
                    message="${this.message}"
                    class="icon-message-first"></cells-icon-message>`:''
          }
        </section>
        
        <section id="footer">
          <section class="buttons-container" slot="buttons">
            ${this.acceptButton?
              html `<bbva-button-default class="btn ${this.primaryButtonClass} ${this.disablePrimaryButton}" id="acceptButton" @click="${this._onAcceptClick}">
                        ${this.acceptButton}
                        ${this.acceptButtonInfo?
                          html `<span>${this.acceptButtonInfo}</span>`:''}
                    </bbva-button-default>`:''}

            ${this.cancelButton?
              html `<bbva-button-default class="link ${this.disableSecondaryButton}" id="cancelButton" @click="${this._onCancelClick}">
                        ${this.cancelButton}
                          ${this.cancelButtonInfo?
                            html `<span>${this.cancelButtonInfo}</span>`:''}
                    </bbva-button-default>`:''}
          </section>
        </section>
      </section>
    `;
  }

  _fireCancel(canceled) {
    this.dispatchEvent(new CustomEvent('cancel', {
      bubbles: true,
      composed: true,
      detail: { cancelFromHeader: canceled }
    }));
    this.show = false;
  }

  _onCancelClick() {
    this._fireCancel(false);
  }

  _onAcceptClick() {
    this.dispatchEvent(new CustomEvent('accept', {
      bubbles: true,
      composed: true
    }));
    this.show = false;
  }
}

// Register the element with the browser
customElements.define(LitAlertBox.is, LitAlertBox);
